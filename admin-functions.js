// admin functions script for Stadtkreation Maps plugin by Johannes Bouchain
var $ = jQuery;

var custom_uploader;
var url_field;
var current_button;
var image_field;

$('.upload_image_button').unbind('click').on('click',function(e) {
 
	e.preventDefault();
	current_button = $(this);
	url_field = $(this).prev();
	if($(this).next().find('img').attr('src')) image_field = $(this).next().find('img');
	
	//If the uploader object has already been created, reopen the dialog
	if (custom_uploader) {
		custom_uploader.open();
		return;
	}

	//Extend the wp.media object
	custom_uploader = wp.media.frames.file_frame = wp.media({
		title: 'Choose Image',
		button: {
			text: 'Choose Image'
		},
		multiple: false
	});

	//When a file is selected, grab the URL and set it as the text field's value
	custom_uploader.on('select', function() {
		attachment = custom_uploader.state().get('selection').first().toJSON();
		url_field.val(attachment.url);
		if(image_field) image_field.attr('src',attachment.url);
		else current_button.after(' &nbsp; <span style="height:30px;max-width:150px;overflow:visible;display:inline-block"><img src="'+attachment.url+'" style="vertical-align:top;margin-top:-3px;max-height:60px;max-width:150px;" alt="" /></span>');
	});

	//Open the uploader dialog
	custom_uploader.open();

});

$(document).ready(function(){
	$('.colorfield').wpColorPicker();
});	
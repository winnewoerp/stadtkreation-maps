STADTKREATION Maps plugin for georeferenced postings.

Version 3.3.2 - 2020-06-21
------------------------

some bugfixes

Version 3.3.1 - 2020-06-21
------------------------

some bugfixes, removed remaining Bootstrap styles

Version 3.3 - 2020-06-21
------------------------

added several features, removed unused Bootstrap

Version 3.2 - 2020-06-21
------------------------

added several features

Version 3.1 - 2020-06-21
------------------------

initial commit
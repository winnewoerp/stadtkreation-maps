var $ = jQuery, extent = new Array(), stamenLayer = new Array(), mapLayer = new Array(), mapboxLayer = new Array(), vectorLayer = new Array(), clusterLayer = new Array(), vectorDataLayer = new Array(), searchVectorLayer = new Array(), map = new Array(), iconSingle = new Array(), iconOverview = new Array(), vectorSingle, vectorSource = new Array(), clusterSource = new Array(), vectorDataSource = new Array(), searchVectorSource = new Array(), popupElement, popup, popoverFunction, taxonomySelectorHTML, geosearchFormHTML, mapNumber = 1, clickedLayer, currentCount, defaultVectorStyle;

$(document).ready(function(){
	var countMaps = 1;
	$('.stadtkreation-maps').each(function(){
		$(this).attr('id','stadtkreation-maps'+countMaps);
		$(this).attr('data-count',countMaps);
		countMaps++;
	});
	
	if($('.stadtkreation-maps-map-number-box').length) {
		setTimeout(function(){
			$('.stadtkreation-maps-map-number-box').hide();
			$('.stadtkreation-maps-map-number-box[data-id="'+selectedTab+'"]').show();
			$('.stadtkreation-maps-number-selector[data-id="'+selectedTab+'"]').addClass('selected');
		},200);
	}
	
	$('#stadtkreation-maps-counter').on('change',function(){
		window.location.href = counterChangedLink.replace('placeholder1',selectedTab).replace('placeholder2',$(this).val());
	});
	
	// multiple map settings handler
	$('.stadtkreation-maps-number-selector').click(function(e){
		e.preventDefault();
		$(this).blur();
		$('.stadtkreation-maps-number-selector').removeClass('selected');
		$('.stadtkreation-maps-map-number-box').hide();
		$('.stadtkreation-maps-map-number-box[data-id="'+$(this).attr('data-id')+'"]').show();
		$('.stadtkreation-maps-number-selector[data-id="'+$(this).attr('data-id')+'"]').addClass('selected');
		$('#stadtkreation-maps-selected-tab').val($(this).attr('data-id'));
		selectedTab = $(this).attr('data-id');
	});
	
	// settings accordion
	if($('.settings-accordion').length) {
		$('.settings-accordion-content').hide();
		$('.settings-accordion .text-before').click(function(){
			$(this).closest('.settings-accordion').find('.settings-accordion-content').slideToggle();
			$(this).closest('.settings-accordion').toggleClass('open');
		});		
	}
	
});

// Stadtkreation Maps options
function stadtkreation_maps_init() {
	// default vector style
	var fillColor = hexToRgb(stadtkreationMapsOptions[mapNumber].defaultFillColor);
	var fillRGBA = 'rgba('+fillColor.r+','+fillColor.g+','+fillColor.b+','+stadtkreationMapsOptions[mapNumber].defaultFillOpacity+')';
	var strokeColor = hexToRgb(stadtkreationMapsOptions[mapNumber].defaultStrokeColor);
	var strokeRGBA = 'rgba('+strokeColor.r+','+strokeColor.g+','+strokeColor.b+','+stadtkreationMapsOptions[mapNumber].defaultStrokeOpacity+')';
	defaultVectorStyle =  new ol.style.Style({
		fill: new ol.style.Fill({ color: fillRGBA}), 
		stroke: new ol.style.Stroke({ color: strokeRGBA, width: stadtkreationMapsOptions[mapNumber].defaultStrokeWidth, lineDash: stadtkreationMapsOptions[mapNumber].defaultStrokeStyle.split(',')}),
		image: new ol.style.Circle({ radius: 7, fill: new ol.style.Fill({ color: fillRGBA }) })
	});
	
	// loop through all Stadtkreation Maps elements
	for(var i = 0; i < $('.stadtkreation-maps').length;i++) {
		
		// define map extent based upon plugin settings
		extent = ol.extent.applyTransform([-180,-90,180,90], ol.proj.getTransform("EPSG:4326", "EPSG:3857"));
			
		// create watercolor and label layers
		stamenLayer[i] = new ol.layer.Group({
			title: '&nbsp;',
			layers: [
				new ol.layer.Tile({
					'title': stadtkreationMapsTexts.layerWatercolor,
					source: new ol.source.Stamen({layer: 'watercolor'}),
					type: 'base'
				}),
				new ol.layer.Tile({
					'title': stadtkreationMapsTexts.layerLabels,
					source: new ol.source.Stamen({layer: 'toner-labels'}),
					type: 'overlay'
				})
			]
		});
		
		// create standard map layer
		mapLayer[i] = new ol.layer.Group({
			title: '&nbsp;',
			layers: [
				new ol.layer.Tile({
					type: 'base',
					'title': stadtkreationMapsTexts.layerStandard,
					source: new ol.source.OSM(),
				})
			],
		});
		
		// create mapbox based layer
		mapboxLayer[i] = new ol.layer.Group({
			title: '&nbsp;',
			layers: [
				new ol.layer.Tile({
					type: 'base',
					'title': (stadtkreationMapsTexts.layerMapbox[mapNumber] ? stadtkreationMapsTexts.layerMapbox[mapNumber] : stadtkreationMapsTexts.layerMapbox[0]),
					source: new ol.source.XYZ({
						url: 'http://api.tiles.mapbox.com/v4/'+stadtkreationMapsOptions[mapNumber].mapboxUserid+'/{z}/{x}/{y}.png?access_token='+stadtkreationMapsOptions[mapNumber].mapboxToken,
						attributions: [new ol.Attribution({ html: ['<a href="https://www.mapbox.com/about/maps/" target="_blank">© Mapbox © OpenStreetMap</a> <a class="mapbox-improve-map" href="https://www.mapbox.com/map-feedback/#joschi81.lhde7mam/0.000/0.000/3" target="_blank">Improve this map</a>'] })]
					})
				})
			],
		});
		
		// create the layers for optional geojson vector data
		vectorDataSource[i] = new Array();
		vectorDataLayer[i] = new Array();
		
		for(var j=0;j<=(stadtkreationMapsOptions[mapNumber].sumVectorLayers-1);j++) {
			vectorDataSource[i][j] = new ol.source.Vector();
			vectorDataLayer[i][j] = new ol.layer.Vector({
				source: vectorDataSource[i][j],
			});
			if(stadtkreationMapsOptions[mapNumber].vectorData[j]) {
			    vectorDataSource[i][j] = new ol.source.Vector({
					url: stadtkreationMapsOptions[mapNumber].vectorData[j],
					projection: 'EPSG:4326',
					format: new ol.format.GeoJSON(),
					cursor: 'default'
				});
			}
			var fillColor = hexToRgb(stadtkreationMapsOptions[mapNumber].vectorDataFillColor[j]);
			if(!fillColor) fillColor = hexToRgb(stadtkreationMapsOptions[mapNumber].defaultFillColor);
			var fillOpacity = stadtkreationMapsOptions[mapNumber].vectorDataFillOpacity[j];
			if(!fillOpacity) fillOpacity = stadtkreationMapsOptions[mapNumber].defaultFillOpacity;
			var fillRGBA = 'rgba('+fillColor.r+','+fillColor.g+','+fillColor.b+','+fillOpacity+')';
			var strokeColor = hexToRgb(stadtkreationMapsOptions[mapNumber].vectorDataStrokeColor[j]);
			if(!strokeColor) strokeColor = hexToRgb(stadtkreationMapsOptions[mapNumber].defaultStrokeColor);
			var strokeRGBA = 'rgba('+strokeColor.r+','+strokeColor.g+','+strokeColor.b+','+stadtkreationMapsOptions[mapNumber].defaultStrokeOpacity+')';
			var strokeWidth = stadtkreationMapsOptions[mapNumber].defaultStrokeWidth;
			if(!strokeWidth) strokeWidth = stadtkreationMapsOptions[mapNumber].vectorDataStrokeWidth[j];
			var strokeLineDash = stadtkreationMapsOptions[mapNumber].vectorDataStrokeStyle[j].split(',');
			if(!strokeLineDash) strokeLineDash = stadtkreationMapsOptions[mapNumber].defaultStrokeStyle.split(',');
			var vectorDataStyle =  new ol.style.Style({
				fill: new ol.style.Fill({ color: fillRGBA }), 
				stroke: new ol.style.Stroke({ color: strokeRGBA, width: strokeWidth, lineDash: strokeLineDash}),
				image: new ol.style.Circle({ radius: 7, fill: new ol.style.Fill({ color: fillRGBA }) })
			});
			vectorDataLayer[i][j] = new ol.layer.Vector({
				name: 'vectorDataLayer'+(j+1),
				source: vectorDataSource[i][j],
				style: vectorDataStyle
			});
		}
		
		// vector data styles
		function vectorDataStyleFunction(feature) {
			var vectorDataStyles = {};
			return [vectorDataStyles];
		}
		
		// create the vector layer for icon overlays
		vectorSource[i] = new ol.source.Vector();
		vectorLayer[i] = new ol.layer.Vector({
			name: 'vectorLayer',
			source: vectorSource[i]
		});
		
		// marker clustering
		/*clusterSource[i] = new ol.source.Cluster({
			distance: 40,
			source: vectorSource[i]
		  });*/
		
		// marker clustering
		/*clusterLayer[i] = new ol.layer.Vector({
			source: clusterSource[i],
			style: function(feature) {
			  var size = feature.get('features').length;
			  if(size > 1) {
				  var style = new ol.style.Style({
					  image: new ol.style.Circle({
						radius: 10,
						stroke: new ol.style.Stroke({
						  color: '#fff'
						}),
						fill: new ol.style.Fill({
						  color: '#3399CC'
						})
					  }),
					  text: new ol.style.Text({
						text: size.toString(),
						fill: new ol.style.Fill({
						  color: '#fff'
						})
					  })
				  });
			  }
			  else var style = feature.get('features')[0].getStyle();
			  return style;
			}
			
		});*/
		
		/* name: 'Icon '+countIcons,
				title: title,
				content: detailText,
				thumbSrc: thumbnail,
				hideDetailLink: noDetailLink,
				permalink: permalink,
				category: category,
				savePosition: new ol.geom.Point(position) */
		
		// create search layer
		searchVectorSource[i] = new ol.source.Vector();
		searchVectorLayer[i] = new ol.layer.Vector({
			source: searchVectorSource[i]
		});
		
		// map settings
		var fullControls = ol.control.defaults();
		var standardZoom = stadtkreationMapsOptions[mapNumber].zoomOverview;
		var mapLayers = [mapLayer[i]];
		if(stadtkreationMapsOptions[mapNumber].type == 'watercolor') mapLayers = [stamenLayer[i]];
		if(stadtkreationMapsOptions[mapNumber].mapboxUserid != '') mapLayers = [mapboxLayer[i]];
		for(var j = 0;j<=(stadtkreationMapsOptions[mapNumber].sumVectorLayers-1);j++) {
			mapLayers.push(vectorDataLayer[i][j]);
		}
		mapLayers.push(vectorLayer[i]);
		mapLayers.push(searchVectorLayer[i]);
				
		// clusterLayer[i] ueberall einfuegen
		// vectorLayer[i].setVisible(false);
		
		// create geosearch form control
		var geosearchFormControl = function(opt_options) {

			var options = opt_options || {};
			
			var element = document.createElement('div');
			element.className = 'ol-geosearch-form ol-unselectable ol-control';
			element.innerHTML = geosearchFormHTML;

			ol.control.Control.call(this, {
			element: element,
			target: options.target
			});

		};
		ol.inherits(geosearchFormControl, ol.control.Control);
			
		if($('#stadtkreation-maps'+(i+1)).hasClass('overview')) {
			// create taxonomy selector control
			var taxonomySelectorControl = function(opt_options) {

				var options = opt_options || {};
				
				var element = document.createElement('div');
				element.className = 'ol-taxonomy-selector ol-unselectable ol-control';
				element.innerHTML = taxonomySelectorHTML;

				ol.control.Control.call(this, {
				element: element,
				target: options.target
				});

			};
			ol.inherits(taxonomySelectorControl, ol.control.Control);
			
			fullControls = ol.control.defaults()
			.extend([
				new ol.control.FullScreen({
					tipLabel: stadtkreationMapsTexts.fullscreenView
				}),
				new taxonomySelectorControl(),
				new geosearchFormControl()
			]);
		}
		
		if($('#stadtkreation-maps'+(i+1)).hasClass('addpost')) {
			fullControls = ol.control.defaults()
				.extend([
					new ol.control.FullScreen({
						tipLabel: stadtkreationMapsTexts.fullscreenView
					}),
					new geosearchFormControl()
				]);
			standardZoom = stadtkreationMapsOptions[mapNumber].zoomSetmarker;
		}
		if($('#stadtkreation-maps'+(i+1)).hasClass('single')) {
			standardZoom = stadtkreationMapsOptions[mapNumber].zoomSingle;
			fullControls = ol.control.defaults()
		}
		
		map[i] = new ol.Map({
			target: 'stadtkreation-maps'+(i+1),
			mapNumber: 'map'+(i+1),
			controls : fullControls,
			interactions: ol.interaction.defaults({mouseWheelZoom:false}),
			layers: mapLayers,
			view: new ol.View({
				center: ol.proj.transform([stadtkreationMapsOptions[mapNumber].lon,stadtkreationMapsOptions[mapNumber].lat], 'EPSG:4326', 'EPSG:3857'),
				zoom: standardZoom,
				minZoom: stadtkreationMapsOptions[mapNumber].minzoom,
				maxZoom: 21,
				extent: extent
			})
		});
		
		if($('#stadtkreation-maps'+(i+1)).hasClass('addpost') || $('#stadtkreation-maps'+(i+1)).hasClass('single') || $('#stadtkreation-maps'+(i+1)).hasClass('settings-map')) {
			iconSingle[i] = new ol.Feature();
			
			var iconStyle = new ol.style.Style({
				image: new ol.style.Icon(({
					anchor: [stadtkreationMapsOptions[mapNumber].iconAnchorX,stadtkreationMapsOptions[mapNumber].iconAnchorY],
					anchorXUnits: 'pixels',
					anchorYUnits: 'pixels',
					src: standardIconUrl[mapNumber]
				}))
			});
			iconSingle[i].setStyle(iconStyle);
			vectorSource[i].addFeature(iconSingle[i]);
			
			
			
			
			if($('#stadtkreation-maps'+(i+1)).hasClass('addpost') || $('#stadtkreation-maps'+(i+1)).hasClass('settings-map')) {
				var elem = map[i], currentCoordinate = '';
				elem.on('singleclick',function(evt){
					targetNumber = evt.map.getTarget().replace('stadtkreation-maps','');
					moveIcon(evt.coordinate,targetNumber);
				});
				$('#stadtkreation-maps'+(i+1)).after('<span class="delete_current_marker'+(i+1)+'" style="display:none"><a href="#">'+stadtkreationMapsTexts.deleteMapPin+'</a></span>');
				
				$('.delete_current_marker'+(i+1)+' a').click(function(e){
					e.preventDefault();
					deleteCurrentIcon(map,$(this).closest('span').attr('class').replace('delete_current_marker',''));
				});
				
			}
			if($('#stadtkreation-maps'+(i+1)).hasClass('addpost')) {
				// add drawing features if setting activated for this map
				
				var dr = this.draw = {};
				var _this = this;
  
				if(stadtkreationMapsOptions[mapNumber].drawingActivated) {
					var $drawing_controls = $('<div class="drawing-controls-wrapper">'), $point = $('<span class="draw-control">').attr('title',stadtkreationMapsTexts.toggleDrawMode), $polygon = $point.clone(), $line = $point.clone(), $remove_features = $point.clone().attr('title',stadtkreationMapsTexts.removeAllFeatures), $modify_features = $point.clone().attr('title',stadtkreationMapsTexts.toggleModify); 
					$drawing_controls.append($('<p>').append($('<strong>').append(stadtkreationMapsTexts.drawingTools)), $point.append(stadtkreationMapsTexts.point), $line.append(stadtkreationMapsTexts.line), $polygon.append(stadtkreationMapsTexts.polygon), $remove_features.append(stadtkreationMapsTexts.clear), $modify_features.append(stadtkreationMapsTexts.modify));
					$('#stadtkreation-maps'+(i+1)).after($drawing_controls);
				
					var drawingSource = new ol.source.Vector();
					var drawingLayer = new ol.layer.Vector({
						name: 'drawingLayer',
						source: drawingSource,
						style: defaultVectorStyle
					});
					map[i].getLayers().insertAt(3,drawingLayer);
					
					// Keep draw interaction and button state in sync:
					var syncBnState = function(int,$bn){
					  var go = function(){ int.getActive() ? $bn.addClass('active') : $bn.removeClass('active'); };
					  go();
					  int.on('change:active', go);
					};
					var drawingFeatures = drawingSource.getFeatures();

					// Create interactions, add them to map - first modify:
					/*mod = this.modify = new ol.interaction.Modify({
						features: drawingFeatures,
						deleteCondition: function(event) {
						  return ol.events.condition.shiftKeyOnly(event) && ol.events.condition.singleClick(event);
						}
					});
					map[i].addInteraction(mod);
					syncBnState(mod,$mod);*/
					var pnt = this.draw.intPoint = new ol.interaction.Draw({
						features: drawingFeatures, 
						type: 'Point'
					}); pnt.setActive(false);
					map[i].addInteraction(pnt);
					syncBnState(pnt,$point);
					var lns = this.intLineString = new ol.interaction.Draw({
						features: drawingFeatures,
						type: 'LineString'
					}); lns.setActive(false);
					map[i].addInteraction(lns);
					syncBnState(lns,$line);
					var pol = this.intPolygon = new ol.interaction.Draw({
						features: drawingFeatures, 
						type: 'Polygon'
					}); pol.setActive(false);
					map[i].addInteraction(pol);
					syncBnState(pol,$polygon);
					
					this.draw.activate = function(int){
					  for(var key in this){
						var INT = this[key];
						if(INT instanceof ol.interaction.Draw){
						  (INT == int && !INT.getActive()) ? INT.setActive(true) : INT.setActive(false);  
						}
					  }
					};
					 
					// Set button handlers:
					$point.click(function(){ _this.draw.activate(pnt); });
					$line.click(function(){ _this.draw.activate(lns); });
					$polygon.click(function(){ _this.draw.activate(pol); });
					$remove_features.click(function(){ // Must run backwards through array. No method for removing multiple features in ol3?
					  var a = drawingFeatures;
					  for(var i=a.length; i>=0; i--){ drawingSource.removeFeature(a[i]); }
					});
					/*$mod.click(function(){
					  mod.getActive() ? mod.setActive(false) : mod.setActive(true);
					});*/
				}
			}
		}
		
		if($('#stadtkreation-maps'+(i+1)).hasClass('overview')) {
			// add empty popup to respective map
			$('#stadtkreation-maps'+(i+1)).append('<div class="popup"><div class="arrow"></div><button class="close" onclick="$(this).closest(\'.popup\').hide()">×</button><h3 class="title"></h3><div class="content"></content></div>');
			var popupElement = $('#stadtkreation-maps'+(i+1)).find('.popup');
			$(popupElement).css('bottom',stadtkreationMapsOptions[mapNumber].popupDistance);
			$(popupElement).css('margin-left',stadtkreationMapsOptions[mapNumber].iconWidth-stadtkreationMapsOptions[mapNumber].iconAnchorX*2);
			$(popupElement).hide();
			var popup = new ol.Overlay({
				element: popupElement[0],
				positioning: 'bottom-center',
				stopEvent: false,
			});
			map[i].addOverlay(popup);
			
			map[i].on('singleclick',function(evt){
				clickedLayer = '';
				var feature = evt.map.forEachFeatureAtPixel(
					evt.pixel,
					function(feature, layer) {
						clickedLayer = layer;
						return feature;
					}
				);
				var featureOrLayer = feature;
				if(clickedLayer) if(clickedLayer.get('permalink')) featureOrLayer = clickedLayer;
				if(featureOrLayer) {
					featureLink = featureOrLayer.get('permalink');
					if(featureLink) {
						$(popupElement).show();
						// set popup position relative to clicked icon or to clicked pixel when single entry vector feature has been clicked
						if(!clickedLayer.get('permalink')) 
						popup.setPosition(featureOrLayer.getGeometry().getCoordinates());
						else {
							var clickedPixel = [evt.pixel[0], evt.pixel[1]+stadtkreationMapsOptions[mapNumber].popupDistance];
							popup.setPosition(evt.map.getCoordinateFromPixel(clickedPixel));
						}
						
						// populate popup
						if(!featureOrLayer.get('hideDetailLink')) $(popupElement).find('.title').html('<a title="'+stadtkreationMapsTexts.showDetails+'" href="'+featureOrLayer.get('permalink')+'">'+featureOrLayer.get('title')+'</a>');
						else $(popupElement).find('.title').html(featureOrLayer.get('title'));
						
						// thumb either with or without link
						var thumbContent = '';
						if(featureOrLayer.get('thumbSrc')) thumbContent = '<img src="'+featureOrLayer.get('thumbSrc')+'" alt="thumbnail" />';
						if(!featureOrLayer.get('hideDetailLink')) $(popupElement).find('.content').html('<p><a title="'+stadtkreationMapsTexts.showDetails+'" href="'+featureOrLayer.get('permalink')+'"><span class="thumb-box">'+thumbContent+'</span></a></p>');
						else $(popupElement).find('.content').html('<p><span class="thumb-box">'+thumbContent+'</span></p>');
						
						// output content if enabled
						//if(featureOrLayer.get('content')) $(popupElement).data('bs.popover').options.content += '<p>'+featureOrLayer.get('content').replace(/(?:\r\n|\r|\n)/g, '<br />')+'</p>';
						if(featureOrLayer.get('content')) $(popupElement).find('.content').html($(popupElement).find('.content').html()+'<p>'+featureOrLayer.get('content')+'</p>');
						
						// show detail link if not disabled
						if(!featureOrLayer.get('hideDetailLink')) $(popupElement).find('.content').html($(popupElement).find('.content').html()+'<p><a href="'+featureOrLayer.get('permalink')+'">'+stadtkreationMapsTexts.showDetails+'</a></p>');
						
						if(!thumbContent) $(popupElement).find('.content').html($(popupElement).find('.content').html().replace('<span class="thumb-box"></span>',''));
						//$(popupElement).popover('show');
						
						// create pan animation
						evt.map.beforeRender(
							ol.animation.pan({
								duration: 500,
								source: (evt.map.getView().getCenter())
							})
						);
						
						// calculate new center position (approx. center of popup)
						if(!clickedLayer.get('permalink')) var pixelCoordinate = evt.map.getPixelFromCoordinate(featureOrLayer.getGeometry().getCoordinates());
						else var pixelCoordinate = evt.map.getPixelFromCoordinate(evt.coordinate);
						var pixelCoordinateNew = [pixelCoordinate[0],pixelCoordinate[1]-$('.popup').innerHeight()/2];
						evt.map.getView().setCenter(evt.map.getCoordinateFromPixel(pixelCoordinateNew));
					}
				}
				else {
					//$(popupElement).popover('destroy');
				}
			})
			
			// change mouse cursor when over marker
			map[i].on('pointermove', function(e) {
			  if (e.dragging) return;
			  var hit = e.map.hasFeatureAtPixel(e.pixel, function(layer){
				 return layer.get('name') === 'vectorLayer' || layer.get('name') === 'singleFeatureLayer'; // boolean
			  });
			  
			  e.map.getTargetElement().style.cursor = hit ? 'pointer' : '';
			});
			
			if($(window).innerWidth()>640) $('.ol-taxonomy-selector').css('left',$('.ol-taxonomy-selector').closest('.stadtkreation-maps').innerWidth()/2-$('.ol-taxonomy-selector').innerWidth()/2);
			
		}
		
		// handle the Stadtkreation Maps searchform
		$('#stadtkreation-maps'+(i+1)+' .ol-geosearch-form button').click(function(){
			if($(this).closest('.ol-geosearch-form').find('form').hasClass('hidden')) $(this).closest('.ol-geosearch-form').find('form').removeClass('hidden');
			else $(this).closest('.ol-geosearch-form').find('form').addClass('hidden');
		});
		$('#stadtkreation-maps'+(i+1)+' .geosearchform').on('submit', function(e) {
			var element = $(this);
			e.preventDefault();
			var counter = Number($(this).closest('.stadtkreation-maps').attr('id').replace('stadtkreation-maps',''))-1;
			searchVectorSource[counter].clear();
			
			var counterTexts = counter;
			var addressField = $(this).find('.addressfield');
			var searchValue = addressField.val();
			
			// TO DO: City name per Map
			if(stadtkreationMapsTexts.cityName) if(searchValue.replace(',','')==searchValue) searchValue = stadtkreationMapsTexts.cityName+', '+searchValue;

			var secondTry = false
			$.getJSON('https://nominatim.openstreetmap.org/search?format=json&q=' + searchValue, function(data) {
				if(data[0]) {
					element.addClass('hidden');
					var foundExtent = data[0].boundingbox;
					var placemarkLat = data[0].lat;
					var placemarkLon = data[0].lon;
									
					var foundMarker = new ol.Feature();
					foundMarker.setGeometry(new ol.geom.Point(ol.proj.transform([Number(placemarkLon), Number(placemarkLat)], 'EPSG:4326', 'EPSG:3857')));
					searchVectorSource[counter].addFeatures([foundMarker]);
				   
					map[counter].getView().setCenter(ol.proj.transform([Number(placemarkLon), Number(placemarkLat)], 'EPSG:4326', 'EPSG:3857'));
				}
				else {
					searchValue = addressField.val();
					$.getJSON('https://nominatim.openstreetmap.org/search?format=json&q=' + searchValue, function(data) {
						if(data[0]) {
							element.addClass('hidden');
							var foundExtent = data[0].boundingbox;
							var placemarkLat = data[0].lat;
							var placemarkLon = data[0].lon;
											
							var foundMarker = new ol.Feature();
							foundMarker.setGeometry(new ol.geom.Point(ol.proj.transform([Number(placemarkLon), Number(placemarkLat)], 'EPSG:4326', 'EPSG:3857')));
							searchVectorSource[counter].addFeatures([foundMarker]);
						   
							map[counter].getView().setCenter(ol.proj.transform([Number(placemarkLon), Number(placemarkLat)], 'EPSG:4326', 'EPSG:3857'));
						}
						else {
							element.find('.addressfield').before('<p class="nothingfound"><strong>'+'nichts gefunden'+'</strong></p>');
							setTimeout(function(){
								element.find('.nothingfound').remove();
							},1500);
						}
					});	
				}
			});
			
			 
		});
		
	}
	for(var i = 0; i < map.length; i++) map[i].updateSize();
}

$(window).resize(function(){
	if($(window).innerWidth()>640) {
		$('.ol-taxonomy-selector .selectbuttons').show();
		$('.ol-taxonomy-selector').css('left',$('.ol-taxonomy-selector').closest('.stadtkreation-maps').innerWidth()/2-$('.ol-taxonomy-selector').innerWidth()/2);
		$('.ol-taxonomy-selector .mobiletoggle').hide();
	}
	else {
		$('.ol-taxonomy-selector').css('left','0.5em');
		$('.ol-taxonomy-selector .selectbuttons').hide();
		$('.ol-taxonomy-selector .mobiletoggle').show();
	}
	for(var i = 0; i < map.length; i++) map[i].updateSize();
});

function moveIcon(position,currentNumber,src,vectorData) {
	var currentMap = map[currentNumber-1];
	positionLonLat = ol.proj.toLonLat(position);
	
	// if click within bounds set in plugin options, animated pan and set marker to clicked position
	var pan = ol.animation.pan({
		duration: 500,
		source: (currentMap.getView().getCenter())
	});
	currentMap.beforeRender(pan);
	currentMap.getView().setCenter(position);
	if(!src) src = standardIconUrl[mapNumber];
	var iconStyle = new ol.style.Style({
		image: new ol.style.Icon(({
			anchor: [stadtkreationMapsOptions[mapNumber].iconAnchorX,stadtkreationMapsOptions[mapNumber].iconAnchorY],
			anchorXUnits: 'pixels',
			anchorYUnits: 'pixels',
			src: src
		}))
	});
	
	iconSingle[currentNumber-1].setStyle(iconStyle);
	iconSingle[currentNumber-1].setGeometry(new ol.geom.Point(position));
	
	if(vectorData!='') {
		currentSource = new ol.source.Vector({
			url: vectorData,
			format: new ol.format.GeoJSON()
		});
		currentLayer = new ol.layer.Vector({
			name: 'singleFeatureLayer',
			source: currentSource,
			style: defaultVectorStyle
		});
		currentMap.getLayers().insertAt(3,currentLayer);
	}
	
	// update position info
	update_position_info(positionLonLat,currentNumber);
}

function update_position_info(position,currentNumber) {
	if(position && $('.stadtkreation_maps_show_current_position'+currentNumber)) $('.stadtkreation_maps_show_current_position'+currentNumber).html(position[0]+", "+position[1]);
	var classAdd = '';
	for(var i = 0;i<2;i++) {
		classAdd = currentNumber;
		if(!$('.stadtkreation_maps_show_current_position'+classAdd).length) classAdd = '';
		if(position) {
			if($('.stadtkreation_maps_show_current_position'+classAdd)) $('.stadtkreation_maps_show_current_position'+classAdd).html(position[0]+", "+position[1]);
			if($('.stadtkreation-maps-position-lon'+classAdd)) $('.stadtkreation-maps-position-lon'+classAdd).val(position[0]);
			if($('.stadtkreation-maps-position-lat'+classAdd)) $('.stadtkreation-maps-position-lat'+classAdd).val(position[1]);
			if($('.delete_current_marker'+classAdd)) $('.delete_current_marker'+classAdd).css('display','inline');
		}
		else {
			if($('.stadtkreation_maps_show_current_position'+classAdd)) $('.stadtkreation_maps_show_current_position'+classAdd).html(stadtkreationMapsTexts.noPositionSet);
			if($('.stadtkreation-maps-position-lon'+classAdd)) $('.stadtkreation-maps-position-lon'+classAdd).val('');
			if($('.stadtkreation-maps-position-lat'+classAdd)) $('.stadtkreation-maps-position-lat'+classAdd).val('');
			if($('.delete_current_marker'+classAdd)) $('.delete_current_marker'+classAdd).css('display','none');
		}
	}
}

function placeCurrentIcon(lon,lat,src,vectorData,currentNumber) {
	$('.reload-marker-position').blur();
	var i = 0;
	var position = ol.proj.transform([lon,lat], 'EPSG:4326', 'EPSG:3857');
	if(!currentNumber) {
		$('.stadtkreation-maps').each(function(){
			if(typeof(map[i])!='undefined')
				if($(this).hasClass('addpost') || $(this).hasClass('single')) moveIcon(position,i+1,src,vectorData);
			i++;
		});
	}
	else moveIcon(position,currentNumber,src,vectorData);
}

function deleteCurrentIcon(map,currentNumber) {
	iconSingle[currentNumber-1].setGeometry();
	update_position_info('',currentNumber);
}

function addSingleMapData(lon,lat,title,countIcons,permalink,category,iconurl,thumbnail,detailText,noDetailLink,vectorData,vectorStyleData) {
	// single vector style
	var fillColor = (vectorStyleData[4] ? hexToRgb(vectorStyleData[4]) : hexToRgb(stadtkreationMapsOptions[mapNumber].defaultFillColor));
	var fillRGBA = 'rgba('+fillColor.r+','+fillColor.g+','+fillColor.b+','+(vectorStyleData[5] ? vectorStyleData[5] : stadtkreationMapsOptions[mapNumber].defaultFillOpacity)+')';
	var strokeColor = (vectorStyleData[0] ? hexToRgb(vectorStyleData[0]) : hexToRgb(stadtkreationMapsOptions[mapNumber].defaultStrokeColor));
	var strokeRGBA = 'rgba('+strokeColor.r+','+strokeColor.g+','+strokeColor.b+','+(vectorStyleData[1] ? vectorStyleData[1] : stadtkreationMapsOptions[mapNumber].defaultStrokeOpacity)+')';
	singleVectorStyle =  new ol.style.Style({
		fill: new ol.style.Fill({ color: fillRGBA}), 
		stroke: new ol.style.Stroke({ color: strokeRGBA, width: (vectorStyleData[2] ? vectorStyleData[2] : stadtkreationMapsOptions[mapNumber].defaultStrokeWidth), lineDash: (vectorStyleData[3] ? vectorStyleData[3].split(',') : stadtkreationMapsOptions[mapNumber].defaultStrokeStyle.split(','))}),
	});
	
	var i = 0;
	var position = ol.proj.transform([parseFloat(lon),parseFloat(lat)], 'EPSG:4326', 'EPSG:3857');
	$('.stadtkreation-maps').each(function(){
		if($(this).hasClass('overview')) {
			if(iconurl=='') iconurl=standardIconUrl[mapNumber];
			iconOverview[countIcons] = new ol.Feature({
				name: 'Icon '+countIcons,
				title: title,
				content: detailText,
				thumbSrc: thumbnail,
				hideDetailLink: noDetailLink,
				permalink: permalink,
				category: category,
				savePosition: new ol.geom.Point(position)
			});
			
			var iconStyle = new ol.style.Style({
			  image: new ol.style.Icon(({
				anchor: [stadtkreationMapsOptions[mapNumber].iconAnchorX,stadtkreationMapsOptions[mapNumber].iconAnchorY],
				anchorXUnits: 'pixels',
				anchorYUnits: 'pixels',
				src: iconurl,
				
			  }))
			});
			
			// set style and position of current icon
			iconOverview[countIcons].setStyle(iconStyle);
			iconOverview[countIcons].setGeometry(new ol.geom.Point(position));
			vectorSource[i].addFeature(iconOverview[countIcons]);
			
			// get and display the vector data
			if(vectorData!='') {
				currentSource = new ol.source.Vector({
					url: vectorData,
					format: new ol.format.GeoJSON()
				});
				currentLayer = new ol.layer.Vector({
					name: 'singleFeatureLayer',
					title: title,
					content: detailText,
					thumbSrc: thumbnail,
					hideDetailLink: noDetailLink,
					permalink: permalink,
					category: category,
					source: currentSource,
					style: singleVectorStyle
				});
				map[i].getLayers().insertAt(3,currentLayer);
			}
			
		}
		i++;
	});
}

function iconSelectCat(elem) {
	$('.popover').hide();
	if(!$(elem).closest('span').find('.off').length) {
		for(var i = 0; i < iconOverview.length; i++) {
			iconOverview[i].setGeometry();
		}
		$(elem).closest('span').find('button').addClass('off');
	}
	if($(elem).hasClass('off')) $(elem).removeClass('off');
	else $(elem).addClass('off');
	for(var i = 0; i < iconOverview.length; i++) {
		 if($(elem).attr('name')==iconOverview[i].get('category')) {
			if(!iconOverview[i].get('geometry')) iconOverview[i].setGeometry(iconOverview[i].get('savePosition'));
			else iconOverview[i].setGeometry();			
		}
	}
}

var taxonomySelectorOpen = false;
function toggleTaxonomySelector() {
	if(taxonomySelectorOpen) {
		$('.ol-taxonomy-selector .selectbuttons').slideUp();
		taxonomySelectorOpen = false;
	}
	else {
		$('.ol-taxonomy-selector .selectbuttons').slideDown();
		taxonomySelectorOpen = true;
	}
}

// handle file upload
var custom_uploader;
var url_field;
var current_button;
var image_field;
$('.upload_image_button').unbind('click').on('click',function(e) {
	e.preventDefault();
	current_button = $(this);
	url_field = $(this).prev();
	if($(this).next().find('img').attr('src')) image_field = $(this).next().find('img');
	
	//If the uploader object has already been created, reopen the dialog
	if (custom_uploader) {
		custom_uploader.open();
		return;
	}

	//Extend the wp.media object
	custom_uploader = wp.media.frames.file_frame = wp.media({
		title: 'Choose Image',
		button: {
			text: 'Choose Image'
		},
		multiple: false
	});

	//When a file is selected, grab the URL and set it as the text field's value
	custom_uploader.on('select', function() {
		attachment = custom_uploader.state().get('selection').first().toJSON();
		url_field.val(attachment.url);
		if(image_field) image_field.attr('src',attachment.url);
		else current_button.after(' &nbsp; <span style="height:30px;max-width:150px;overflow:visible;display:inline-block"><img src="'+attachment.url+'" style="vertical-align:top;margin-top:-3px;max-height:60px;max-width:150px;" alt="" /></span>');
	});

	//Open the uploader dialog
	custom_uploader.open();

});

function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}
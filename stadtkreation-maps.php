<?php
/*
Plugin Name: Stadtkreation Maps
Plugin URI: http://www.stadtkreation.de/wordpress-plugins-und-themes
Description: Stadtkreation Maps plugin for georeferenced postings.
Version: 3.3.2
Author: Johannes Bouchain
Text Domain: stadtkreation-maps
Domain Path: /languages/
Author URI: http://www.stadtkreation.de/ueber-uns
*/
__('Stadtkreation Maps plugin for georeferenced postings.','stadtkreation-maps');

global $sum_vector_layers;
$sum_vector_layers = 15;

// plugin constants and globals
define ('STADTKREATON_MAPS_PLUGIN_BASENAME',plugin_basename(dirname(__FILE__)));
define ('STADTKREATON_MAPS_PLUGIN_URL',WP_PLUGIN_URL."/".STADTKREATON_MAPS_PLUGIN_BASENAME);
global $is_overview, $overview_map_cat, $map_number, $count_maps, $field_defaults;

// defining fields defaults
$field_defaults = array(
	'zoom-setmarker' => 14,
	'zoom-single' => 15,
	'zoom-overview' => 12,
	'minzoom' => 1,
	// map center defaults to Hamburg - Stadtkreation Maps has been developed here
	'lon' => 9.992773532867432,
	'lat' => 53.55061052339076,
	'type' => 'standard',
	'icon-height' => 33,
	'icon-width' => 23,
	'icon-anchor-x' => 11,
	'icon-anchor-y' => 33,
	'popup-distance' => 30,
	'fill-color' => '#ff0000',
	'fill-opacity' => .3,
	'stroke-color' => '#ff0000',
	'stroke-opacity' => .8,
	'stroke-width' => 3,
);

// get number of maps set in plugin options
if(get_option('stadtkreation-maps-counter')) $count_maps = get_option('stadtkreation-maps-counter');
else $count_maps = 1;

// plugin textdomain definition
function stadtkreation_maps_textdomain() {
	$plugin_dir = basename(dirname(__FILE__));
	load_plugin_textdomain( 'stadtkreation-maps', false, $plugin_dir.'/languages/' );
}
add_action('plugins_loaded', 'stadtkreation_maps_textdomain');

//Stadtkreation Maps enqueue general scripts and styles
function stadtkreation_maps_enqueue_styles() {
	wp_enqueue_style( 'dashicons' );
	wp_enqueue_style( 'stadtkreation-maps-styles-openlayers', STADTKREATON_MAPS_PLUGIN_URL.'/openlayers/ol.css' );
	wp_enqueue_style( 'stadtkreation-maps-styles-openlayers-layerswitcher', STADTKREATON_MAPS_PLUGIN_URL.'/openlayers/ol3-layerswitcher.css' );
	wp_enqueue_style( 'stadtkreation-maps-styles', STADTKREATON_MAPS_PLUGIN_URL.'/stadtkreation-maps.css' );
	
	wp_enqueue_script( 'jquery');
	wp_enqueue_script( 'stadtkreation-maps-bootstrap', STADTKREATON_MAPS_PLUGIN_URL.'/bootstrap.min.js', array(), '20161207', true );
	wp_enqueue_script( 'stadtkreation-maps-openlayers', STADTKREATON_MAPS_PLUGIN_URL.'/openlayers/ol.js', array(), '20161207', true );
	wp_enqueue_script( 'stadtkreation-maps-openlayers-layerswitcher', STADTKREATON_MAPS_PLUGIN_URL.'/openlayers/ol3-layerswitcher.js', array(), '20161207', true );
}
add_action('wp_enqueue_scripts','stadtkreation_maps_enqueue_styles');
add_action('admin_enqueue_scripts','stadtkreation_maps_enqueue_styles');

// loadStadtkreation Maps header scripts
function stadtkreation_maps_script() {
	global $sum_vector_layers, $count_maps, $post, $field_defaults;
	$output = '';
	$stadtkreation_maps_setting = get_option('stadtkreation-maps-settings');

	//$use_drawing_tools = false;
	// for($i=1;$i<=$count_maps;$i++) if(isset($stadtkreation_maps_setting['drawing-activated']) && $stadtkreation_maps_setting['drawing-activated'][$i]) $use_drawing_tools = true;
	
	// outputStadtkreation Maps script
	$output .= '<script type="text/javascript"> ';

	$stadtkreation_maps_setting = get_option('stadtkreation-maps-settings');
	
	// set general variables
	if(is_admin() && !$post->ID) $output .= 'var settingsMapLon = new Array(), settingsMapLat = new Array(); ';
	$output .= 'var siteUrl = "'.get_bloginfo('url').'", stadtkreationMapsOptions = new Array(), standardIconUrl = new Array();'."\r\n";
	$loop_texts = array('city-name','mapbox-layer-name');
	foreach($loop_texts as $current_name) {
		for($i=1;$i<=get_option('stadtkreation-maps-counter');$i++) {
			$text_output[$current_name] .= '"'.$stadtkreation_maps_setting[$current_name][$i].'"';
			if($i<get_option('stadtkreation-maps-counter')) $text_output[$current_name] .= ',';
		}
		$text_output[$current_name] = '['.$text_output[$current_name].']';
	}

	$output .= 'var stadtkreationMapsTexts = {
cityName: '.$text_output['city-name'].',
layerWatercolor: "'.__('Watercolor','stadtkreation-maps').'",
layerLabels: "'.__('Labels','stadtkreation-maps').'",
layerStandard: "'.__('OSM standard map','stadtkreation-maps').'",
layerMapbox: '.$text_output['mapbox-layer-name'].',
fullscreenView: "'.__('Fullscreen map','stadtkreation-maps').'",
showDetails: "'.__('Detailed view','stadtkreation-maps').'",
noPositionSet: "'.__('No position set','stadtkreation-maps').'",
deleteMapPin: "'.__('Delete map pin','stadtkreation-maps').'",
toggleDrawMode: "'.__('Toggle draw mode','stadtkreation-maps').'",
drawingTools: "'.__('Drawing tools','stadtkreation-maps').'",
removeAllFeatures: "'.__('Remove all features','stadtkreation-maps').'",
point: "'.__('Point','stadtkreation-maps').'",
line: "'.__('Line','stadtkreation-maps').'",
polygon: "'.__('Polygon','stadtkreation-maps').'",
clear: "'.__('Clear','stadtkreation-maps').'",
modify: "'.__('Modify','stadtkreation-maps').'"'."\r\n";
	$output .= '}; '."\r\n";
	
	// set variables per used map
	for($i=1;$i<=$count_maps;$i++) {
		if(isset($stadtkreation_maps_setting['default-icon'][$i]) && $stadtkreation_maps_setting['default-icon'][$i] != '') $standardIconUrl = $stadtkreation_maps_setting['default-icon'][$i];
		else $standardIconUrl = get_bloginfo('url').'/wp-content/plugins/stadtkreation-maps/images/pin.png';
		$output .= 'standardIconUrl['.$i.'] = "'.$standardIconUrl.'";'."\r\n";
		$output .= 'stadtkreationMapsOptions['.$i.'] = {
lon: '.(isset($stadtkreation_maps_setting['map-center']) && $stadtkreation_maps_setting['map-center'][$i]['lon'] ? $stadtkreation_maps_setting['map-center'][$i]['lon'] : $field_defaults['lon']).',
lat: '.(isset($stadtkreation_maps_setting['map-center']) && $stadtkreation_maps_setting['map-center'][$i]['lat'] ? $stadtkreation_maps_setting['map-center'][$i]['lat'] : $field_defaults['lat']).',
zoomSetmarker: '.(isset($stadtkreation_maps_setting['zoom-setmarker']) && $stadtkreation_maps_setting['zoom-setmarker'][$i] ? $stadtkreation_maps_setting['zoom-setmarker'][$i] : $field_defaults['zoom-setmarker']).',
zoomSingle: '.(isset($stadtkreation_maps_setting['zoom-single']) && $stadtkreation_maps_setting['zoom-single'][$i] ? $stadtkreation_maps_setting['zoom-single'][$i] : $field_defaults['zoom-single']).',
zoomOverview: '.(isset($stadtkreation_maps_setting['zoom-overview']) && $stadtkreation_maps_setting['zoom-overview'][$i] ? $stadtkreation_maps_setting['zoom-overview'][$i] : $field_defaults['zoom-overview']).',
minzoom: '.(isset($stadtkreation_maps_setting['minzoom']) && $stadtkreation_maps_setting['minzoom'][$i] ? $stadtkreation_maps_setting['minzoom'][$i] : $field_defaults['minzoom']).',
type: "'.(isset($stadtkreation_maps_setting['type']) && $stadtkreation_maps_setting['type'][$i] ? $stadtkreation_maps_setting['type'][$i] : $field_defaults['type']).'",
typePostform: "'.(isset($stadtkreation_maps_setting['type-addpost']) && $stadtkreation_maps_setting['type-addpost'][$i] ? $stadtkreation_maps_setting['type-addpost'][$i] : $field_defaults['typeAddpost']).'",
mapboxUserid: "'.(isset($stadtkreation_maps_setting['mapbox-userid']) ? $stadtkreation_maps_setting['mapbox-userid'][$i] : '').'",
mapboxToken: "'.(isset($stadtkreation_maps_setting['mapbox-access-token']) ? $stadtkreation_maps_setting['mapbox-access-token'][$i] : '').'",
iconHeight: '.(isset($stadtkreation_maps_setting['icon-height']) && $stadtkreation_maps_setting['icon-height'][$i] ? $stadtkreation_maps_setting['icon-height'][$i] : $field_defaults['icon-height']).',
iconWidth: '.(isset($stadtkreation_maps_setting['icon-width']) && $stadtkreation_maps_setting['icon-width'][$i] ? $stadtkreation_maps_setting['icon-width'][$i] : $field_defaults['icon-width']).',
iconAnchorX: '.(isset($stadtkreation_maps_setting['icon-anchor-x']) && $stadtkreation_maps_setting['icon-anchor-x'][$i] ? $stadtkreation_maps_setting['icon-anchor-x'][$i] : $field_defaults['icon-anchor-x']).',
iconAnchorY: '.(isset($stadtkreation_maps_setting['icon-anchor-y']) && $stadtkreation_maps_setting['icon-anchor-y'][$i] ? $stadtkreation_maps_setting['icon-anchor-y'][$i] : $field_defaults['icon-anchor-y']).',
popupDistance: '.(isset($stadtkreation_maps_setting['popup-distance']) && $stadtkreation_maps_setting['popup-distance'][$i] ? $stadtkreation_maps_setting['popup-distance'][$i] : $field_defaults['popup-distance']).',
vectorData: ["';
for($j=1;$j<=$sum_vector_layers;$j++) $output .= (isset($stadtkreation_maps_setting['vector-data']) ? $stadtkreation_maps_setting['vector-data'][$i][$j] : '').'","';
$output .= '"],
vectorDataStrokeWidth: ["';
for($j=1;$j<=$sum_vector_layers;$j++) $output .= (isset($stadtkreation_maps_setting['vector-data-stroke-width']) ? $stadtkreation_maps_setting['vector-data-stroke-width'][$i][$j] : '').'","';
$output .= '"],
vectorDataStrokeColor: ["';
for($j=1;$j<=$sum_vector_layers;$j++) $output .= (isset($stadtkreation_maps_setting['vector-data-stroke-color']) ? $stadtkreation_maps_setting['vector-data-stroke-color'][$i][$j] : '').'","';
$output .= '"],
vectorDataStrokeOpacity: ["';
for($j=1;$j<=$sum_vector_layers;$j++) $output .= (isset($stadtkreation_maps_setting['vector-data-stroke-opacity']) ? $stadtkreation_maps_setting['vector-data-stroke-opacity'][$i][$j] : '').'","';
$output .= '"],
vectorDataStrokeStyle: ["';
for($j=1;$j<=$sum_vector_layers;$j++) $output .= (isset($stadtkreation_maps_setting['vector-data-stroke-style']) ? $stadtkreation_maps_setting['vector-data-stroke-style'][$i][$j] : '').'","';
$output .= '"],
vectorDataFillColor: ["';
for($j=1;$j<=$sum_vector_layers;$j++) $output .= (isset($stadtkreation_maps_setting['vector-data-fill-color']) ? $stadtkreation_maps_setting['vector-data-fill-color'][$i][$j] : '').'","';
$output .= '"],
vectorDataFillOpacity: ["';
for($j=1;$j<=$sum_vector_layers;$j++) $output .= (isset($stadtkreation_maps_setting['vector-data-fill-opacity']) ? $stadtkreation_maps_setting['vector-data-fill-opacity'][$i][$j] : '').'","';
$output .= '"],
defaultStrokeWidth: '.(isset($stadtkreation_maps_setting['default-stroke-width']) && $stadtkreation_maps_setting['default-stroke-width'][$i] ? $stadtkreation_maps_setting['default-stroke-width'][$i] : $field_defaults['stroke-width']).',
defaultStrokeColor: "'.(isset($stadtkreation_maps_setting['default-stroke-color']) && $stadtkreation_maps_setting['default-stroke-color'][$i] ? $stadtkreation_maps_setting['default-stroke-color'][$i] : $field_defaults['stroke-color']).'",
defaultStrokeStyle: "'.(isset($stadtkreation_maps_setting['default-stroke-style']) ? $stadtkreation_maps_setting['default-stroke-style'][$i] : '').'",
defaultStrokeOpacity: '.(isset($stadtkreation_maps_setting['default-stroke-style']) && $stadtkreation_maps_setting['default-stroke-opacity'][$i] ? $stadtkreation_maps_setting['default-stroke-opacity'][$i] : $field_defaults['stroke-opacity']).',
defaultFillColor: "'.(isset($stadtkreation_maps_setting['default-fill-color']) && $stadtkreation_maps_setting['default-fill-color'][$i] ? $stadtkreation_maps_setting['default-fill-color'][$i] : $field_defaults['fill-color']).'",
defaultFillOpacity: '.(isset($stadtkreation_maps_setting['default-fill-opacity']) && $stadtkreation_maps_setting['default-fill-opacity'][$i] ? $stadtkreation_maps_setting['default-fill-opacity'][$i] : $field_defaults['fill-opacity']).',
// drawingActivated: '.(isset($stadtkreation_maps_setting['drawing-activated']) && $stadtkreation_maps_setting['drawing-activated'][$i] ? 'true' : 'false').',
sumVectorLayers: '.$sum_vector_layers.', 
};'."\r\n";
	}
	$output .= '</script>';
	$output .= '<script type="text/javascript" src="'.STADTKREATON_MAPS_PLUGIN_URL.'/stadtkreation-maps.js"></script>';

	
	// output contents
	echo $output;
}
add_action('wp_head', 'stadtkreation_maps_script');
add_action('admin_head', 'stadtkreation_maps_script');

// add Stadtkreation Maps preferences menu
add_action('admin_menu', 'stadtkreation_maps_create_menu');

// add options
add_option('stadtkreation-maps-counter');
add_option('stadtkreation-maps-temp');
add_option('stadtkreation-maps-settings');

// register options
function stadtkreation_maps_register_settings() {
	global $count_maps;
	register_setting('stadtkreation-maps-settings-group','stadtkreation-maps-counter');
	register_setting('stadtkreation-maps-settings-group','stadtkreation-maps-temp');
	register_setting('stadtkreation-maps-settings-group','stadtkreation-maps-settings');
}

// options page
function stadtkreation_maps_settings_page() {
	global $sum_vector_layers, $count_maps, $stadtkreation_maps_fields, $field_defaults;
	echo '<div class="wrap">'."\n";
	echo '<h2>'.__('Preferences Stadtkreation Maps module','stadtkreation-maps').'</h2>'."\n";
	echo '<form method="post" action="options.php">'."\n\t";
	
	settings_fields('stadtkreation-maps-settings-group' );
	do_settings_sections('stadtkreation-maps-settings-group');
	
	$output = '';
	$stadtkreation_maps_setting = get_option('stadtkreation-maps-settings');
	
	// define the reload link for changed counter selection
	$current_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	$counter_changed_link = add_query_arg(array('selected-tab' => 'placeholder1','mapcounter' =>'placeholder2'),$current_link);
	
	// saving reload link and saved tab info for to JS variable
	$output .= '<script type="text/javascript"> var selectedTab = ';
	$stadtkreation_maps_temp = get_option('stadtkreation-maps-temp');
	if($_GET['selected-tab']) $output .= $_GET['selected-tab'];
	elseif(isset($stadtkreation_maps_temp['selected-tab'])) $output .= $stadtkreation_maps_temp['selected-tab'];
	else $output .= '1';
	$output .= '; var counterChangedLink = "'.$counter_changed_link.'";';
	$output .= '</script>';
	delete_option('stadtkreation-maps-temp');
	
	$stadtkreation_maps_counter = ($_GET['mapcounter'] ? $_GET['mapcounter'] : get_option('stadtkreation-maps-counter'));
	if(!$stadtkreation_maps_counter) $stadtkreation_maps_counter = 1;
	
	// count number of maps to be used
	$stadtkreation_maps_counter_field = array('type' => 'select','name' => 'counter','label' => __('How many different map settings do you want to use?','stadtkreation-maps'),'default' => 1,'reload' => true);
	for($i=1;$i<=10;$i++) $stadtkreation_maps_counter_field ['options'][] = array('value' => $i,'name' => $i,);
	$output .= '<p><label for="stadtkreation-maps-counter">'. __('How many maps do you want to use?','stadtkreation-maps').'<br /><select name="stadtkreation-maps-counter" id="stadtkreation-maps-counter">';
	foreach($stadtkreation_maps_counter_field['options'] as $field_option) $output .= '<option '.($stadtkreation_maps_counter==$field_option['value'] ? ' selected="selected"' : '').' value="'.$field_option['value'].'">'.$field_option['name'].'</option>';
	$output .= '</select></label></p>';
	
	// define map types array
	$map_types = array(array('value'=>'watercolor','name'=>'Watercolor'),array('value'=>'standard','name'=>'Standard'),array('value'=>'mapbox','name'=>'Mapbox'));
	
	// get number of used map settings
	$map_number = $stadtkreation_maps_counter;
	
	// defining allStadtkreation Maps options fields
	$stadtkreation_maps_fields[] = array('name' => 'map-center','type' => 'map-poi','label' => __('Please set the center for this map setting','stadtkreation-maps'),'default' => array($field_defaults['lon'],$field_defaults['lat']),'title-before' => __('Center coordinates for map setting %s','stadtkreation-maps'));
	$stadtkreation_maps_fields[] = array('name' => 'zoom-overview','type' => 'select','label' => __('Standard zoom level for overview map','stadtkreation-maps'),'default' => $field_defaults['zoom-overview'],'title-before' => __('Zoom level definitions for map setting %s','stadtkreation-maps')); for($i=0;$i<=21;$i++) $stadtkreation_maps_fields[sizeof($stadtkreation_maps_fields)-1]['options'][] = array('value' => $i,'name' => $i);
	$stadtkreation_maps_fields[] = array('name' => 'zoom-setmarker','type' => 'select','label' => __('Standard zoom level for marker positioning map','stadtkreation-maps'),'default' => $field_defaults['zoom-setmarker']); for($i=0;$i<=21;$i++) $stadtkreation_maps_fields[sizeof($stadtkreation_maps_fields)-1]['options'][] = array('value' => $i,'name' => $i,);
	$stadtkreation_maps_fields[] = array('name' => 'zoom-single','type' => 'select','label' => __('Standard zoom level for single view','stadtkreation-maps'),'default' => $field_defaults['zoom-single']); for($i=0;$i<=21;$i++) $stadtkreation_maps_fields[sizeof($stadtkreation_maps_fields)-1]['options'][] = array('value' => $i,'name' => $i,);
	$stadtkreation_maps_fields[] = array('name' => 'minzoom','type' => 'select','label' => __('Minimal zoom level','stadtkreation-maps'),'default' => 9); for($i=0;$i<=21;$i++) $stadtkreation_maps_fields[sizeof($stadtkreation_maps_fields)-1]['options'][] = array('value' => $i,'name' => $i,);
	$stadtkreation_maps_fields[] = array('name' => 'type','type' => 'select','label' => __('Standard map type','stadtkreation-maps'),'default' => $field_defaults['type'],'options' => $map_types,'title-before' => __('Map type definitions for map setting %s','stadtkreation-maps'));
	$stadtkreation_maps_fields[] = array('name' => 'type-addpost','type' => 'select','label' => __('Standard map type for post form map','stadtkreation-maps'),'default' => $field_defaults['type'],'options' => $map_types);
	$stadtkreation_maps_fields[] = array('name' => 'mapbox-userid','type' => 'input','label' => __('Mapbox user ID','stadtkreation-maps'),'text-before' => __('To use a mapbox layer, type your mapbox user ID, the access token, and your preferred layer title here','stadtkreation-maps'));
	$stadtkreation_maps_fields[] = array('name' => 'mapbox-access-token','type' => 'input','label' => __('Mapbox access token','stadtkreation-maps'));
	$stadtkreation_maps_fields[] = array('name' => 'mapbox-layer-name','type' => 'input','label' => __('Mapbox layer name','stadtkreation-maps'));
	$stadtkreation_maps_fields[] = array('name' => 'city-name','type' => 'input','label' => __('If your map is focussed on a city (or a country), enter the name here (needed for map search)','stadtkreation-maps'));
	$stadtkreation_maps_fields[] = array('name' => 'icon-height','type' => 'input','label' => __('Map icon height (defaults to 33)','stadtkreation-maps'),'default' => 33,'title-before' => __('Icons size and position for map setting %s','stadtkreation-maps'));
	$stadtkreation_maps_fields[] = array('name' => 'icon-width','type' => 'input','label' => __('Map icon width (defaults to 23)','stadtkreation-maps'),'default' => 23);
	$stadtkreation_maps_fields[] = array('name' => 'icon-anchor-x','type' => 'input','label' => __('X value of icon anchor position','stadtkreation-maps'),'default' => 11,'text-before' => __('Set the following values if the arrow of your icon is not located at bottom center.','stadtkreation-maps'));
	$stadtkreation_maps_fields[] = array('name' => 'icon-anchor-y','type' => 'input','label' => __('Y value of icon anchor position','stadtkreation-maps'),'default' => 33);
	$stadtkreation_maps_fields[] = array('name' => 'popup-distance','type' => 'input','label' => __('Vertical distance between icon anchor and popup arrow (defaults to 30)','stadtkreation-maps'),'default' => 30);
	$stadtkreation_maps_fields[] = array('name' => 'drawing-activated','type' => 'checkbox','label' => __('Enable drawing tools for this map','stadtkreation-maps'),'default' => 30,'title-after' => __('Optional vector data for map setting %s','stadtkreation-maps'));
	for($i=1;$i<=$sum_vector_layers;$i++) {
		$stadtkreation_maps_fields[] = array('name' => 'vector-data','type' => 'file','label' => sprintf(__('GeoJSON data for optional vector layer %s','stadtkreation-maps'),$i),'object' => $i,'accordion'=>'start','text-before' => sprintf(__('Settings for vector data %s','stadtkreation-maps'),$i));
		$stadtkreation_maps_fields[] = array('name' => 'vector-data-stroke-color','type' => 'color','label' => sprintf(__('Stroke color for vector layer %s','stadtkreation-maps'),$i),'object' => $i);
		$stadtkreation_maps_fields[] = array('name' => 'vector-data-stroke-opacity','type' => 'select','label' => sprintf(__('Stroke opacity for vector layer %s','stadtkreation-maps'),$i),'object' => $i,'default'=>1); for($j=10;$j>=0;$j--) $stadtkreation_maps_fields[sizeof($stadtkreation_maps_fields)-1]['options'][] = array('value' => $j/10,'name' => $j/10);
		$stadtkreation_maps_fields[] = array('name' => 'vector-data-stroke-width','type' => 'input','label' => sprintf(__('Stroke width for vector layer %s','stadtkreation-maps'),$i),'object' => $i,'default'=>1);
		$stadtkreation_maps_fields[] = array('name' => 'vector-data-stroke-style','type' => 'input','label' => sprintf(__('Stroke style (comma-separated numbers) for vector layer %s','stadtkreation-maps'),$i),'object' => $i);
		$stadtkreation_maps_fields[] = array('name' => 'vector-data-fill-color','type' => 'color','label' => sprintf(__('Fill color for vector layer %s','stadtkreation-maps'),$i),'object' => $i);
		$stadtkreation_maps_fields[] = array('name' => 'vector-data-fill-opacity','type' => 'select','label' => sprintf(__('Fill opacity for vector layer %s','stadtkreation-maps'),$i),'object' => $i,'default'=>1,'accordion' => 'end'); for($j=10;$j>=0;$j--) $stadtkreation_maps_fields[sizeof($stadtkreation_maps_fields)-1]['options'][] = array('value' => $j/10,'name' => $j/10);
	}
	$stadtkreation_maps_fields[] = array('name' => 'posttypes','type' => 'select-multiple','label' => __('Select post types to include','stadtkreation-maps'),'title-before' => __('Post types and taxonomies for map setting %s','stadtkreation-maps'));
	// posttypes objects
	$posttype = get_post_type_object('post'); 
	$stadtkreation_maps_fields[sizeof($stadtkreation_maps_fields)-1]['options'][] = array('value' => $posttype->name,'name' => $posttype->labels->menu_name);
	$posttypes = get_post_types(array('_builtin'=>false),'objects');
	foreach($posttypes as $posttype) $stadtkreation_maps_fields[sizeof($stadtkreation_maps_fields)-1]['options'][] = array('value' => $posttype->name,'name' => $posttype->labels->menu_name);
	
	
	for($i=1;$i<=$map_number;$i++) {
		if(isset($stadtkreation_maps_setting['posttypes'][$i])) {
		foreach($stadtkreation_maps_setting['posttypes'][$i] as $posttype) {
			$posttype_object = get_post_type_object($posttype); 
			$stadtkreation_maps_fields[] = array('name' => 'posttype-popover-content','type' => 'checkbox','label' => sprintf(__('Show content for "%s" post type','stadtkreation-maps'),$posttype_object->labels->menu_name),'object' => $posttype,'counter' => $i);
			$stadtkreation_maps_fields[] = array('name' => 'posttype-popover-nodetails','type' => 'checkbox','label' => sprintf(__('Hide detail page link for "%s" post type','stadtkreation-maps'),$posttype_object->labels->menu_name),'object' => $posttype,'counter' => $i);
		}
	}
	}
	
	$stadtkreation_maps_fields[] = array('name' => 'taxonomy','type' => 'select','label' => __('Select taxonomy to include','stadtkreation-maps'));
	// taxonomy objects
	$taxonomy = get_taxonomy('category');
	$stadtkreation_maps_fields[sizeof($stadtkreation_maps_fields)-1]['options'][] = array('value' => $taxonomy->name,'name' => $taxonomy->labels->menu_name);
	$taxonomies= get_taxonomies(array('_builtin'=>false),'objects');
	foreach($taxonomies as $taxonomy) $stadtkreation_maps_fields[sizeof($stadtkreation_maps_fields)-1]['options'][] = array('value' => $taxonomy->name,'name' => $taxonomy->labels->menu_name);
	
	$stadtkreation_maps_fields[] = array('name' => 'taxonomy-usedesc','type' => 'checkbox','label' => __('Use taxonomy description instead of name','stadtkreation-maps'));
	$stadtkreation_maps_fields[] = array('name' => 'taxonomy-others','type' => 'checkbox','label' => __('Show "Others" button in term selector','stadtkreation-maps'));
	$stadtkreation_maps_fields[] = array('name' => 'default-icon','type' => 'image','label' => __('Default icon file','stadtkreation-maps'),'title-before' => __('Settings per term for map setting %s','stadtkreation-maps'),'text-before' => __('Default term settings','stadtkreation-maps'),'accordion' => 'start');
	$stadtkreation_maps_fields[] = array('name' => 'default-stroke-color','type' => 'color','label' => sprintf(__('Stroke color','stadtkreation-maps'),$taxonomy_object->name),'default'=>$field_defaults['stroke-color']);
	$stadtkreation_maps_fields[] = array('name' => 'default-stroke-opacity','type' => 'select','label' => sprintf(__('Stroke opacity','stadtkreation-maps'),$taxonomy_object->name),'default'=>$field_defaults['stroke-opacity']); for($j=10;$j>=0;$j--) $stadtkreation_maps_fields[sizeof($stadtkreation_maps_fields)-1]['options'][] = array('value' => $j/10,'name' => $j/10);
	$stadtkreation_maps_fields[] = array('name' => 'default-stroke-width','type' => 'input','label' => sprintf(__('Stroke width','stadtkreation-maps'),$taxonomy_object->name),'default'=>$field_defaults['stroke-width']);
	$stadtkreation_maps_fields[] = array('name' => 'default-stroke-style','type' => 'input','label' => sprintf(__('Stroke style (comma-separated numbers)','stadtkreation-maps'),$taxonomy_object->name));
	$stadtkreation_maps_fields[] = array('name' => 'default-fill-color','type' => 'color','label' => sprintf(__('Fill color','stadtkreation-maps'),$taxonomy_object->name),'default'=>$field_defaults['fill-color']);
	$stadtkreation_maps_fields[] = array('name' => 'default-fill-opacity','type' => 'select','label' => sprintf(__('Fill opacity','stadtkreation-maps'),$taxonomy_object->name),'default'=>$field_defaults['fill-opacity'],'accordion' => 'end'); for($j=10;$j>=0;$j--) $stadtkreation_maps_fields[sizeof($stadtkreation_maps_fields)-1]['options'][] = array('value' => $j/10,'name' => $j/10);
	
	for($i=1;$i<=$map_number;$i++) {
		$taxonomy_objects = get_categories( 'hide_empty=0&tab_index=4&taxonomy='.(isset($stadtkreation_maps_setting['taxonomy']) ? $stadtkreation_maps_setting['taxonomy'][$i] : '').'&orderby=slug' );
		foreach($taxonomy_objects as $taxonomy_object) {
			$stadtkreation_maps_fields[] = array('name' => 'taxonomy-icon','type' => 'image','label' => sprintf(__('Icon file for "%s"','stadtkreation-maps'),$taxonomy_object->name),'object' => $taxonomy_object->term_id,'counter' => $i,'text-before' => sprintf(__('Settings for "%s"','stadtkreation-maps'),$taxonomy_object->name),'accordion' => 'start');
			$stadtkreation_maps_fields[] = array('name' => 'taxonomy-stroke-color','type' => 'color','label' => sprintf(__('Stroke color for "%s"','stadtkreation-maps'),$taxonomy_object->name),'object' => $taxonomy_object->term_id,'counter' => $i);
			$stadtkreation_maps_fields[] = array('name' => 'taxonomy-stroke-opacity','type' => 'select','label' => sprintf(__('Stroke opacity for "%s"','stadtkreation-maps'),$taxonomy_object->name),'object' => $taxonomy_object->term_id,'counter' => $i); for($j=10;$j>=0;$j--) $stadtkreation_maps_fields[sizeof($stadtkreation_maps_fields)-1]['options'][] = array('value' => $j/10,'name' => $j/10);
			$stadtkreation_maps_fields[] = array('name' => 'taxonomy-stroke-width','type' => 'input','label' => sprintf(__('Stroke width for "%s"','stadtkreation-maps'),$taxonomy_object->name),'object' => $taxonomy_object->term_id,'counter' => $i,'default'=>1);
			$stadtkreation_maps_fields[] = array('name' => 'taxonomy-stroke-style','type' => 'input','label' => sprintf(__('Stroke style (comma-separated numbers) for "%s"','stadtkreation-maps'),$taxonomy_object->name),'object' => $taxonomy_object->term_id,'counter' => $i);
			$stadtkreation_maps_fields[] = array('name' => 'taxonomy-fill-color','type' => 'color','label' => sprintf(__('Fill color for "%s"','stadtkreation-maps'),$taxonomy_object->name),'object' => $taxonomy_object->term_id,'counter' => $i);
			$stadtkreation_maps_fields[] = array('name' => 'taxonomy-fill-opacity','type' => 'select','label' => sprintf(__('Fill opacity for "%s"','stadtkreation-maps'),$taxonomy_object->name),'object' => $taxonomy_object->term_id,'counter' => $i,'default'=>1,'accordion' => 'end'); for($j=10;$j>=0;$j--) $stadtkreation_maps_fields[sizeof($stadtkreation_maps_fields)-1]['options'][] = array('value' => $j/10,'name' => $j/10);
		}
	}
	
	// output top submit button
	$output .= '<p class="submit"><input type="submit" class="button-primary" value="'.__('Save Changes').'" /></p>'."\n";
			
	// map number selector
	$output .= '<p class="stadtkreation-maps-map-number-selector">';
	
	for($i=1;$i<=$map_number;$i++) $output .= '<a href="#" class="stadtkreation-maps-number-selector" data-id="'.$i.'">'.$i.'</a>';
	$output .= '</p>';
	
	$countSettingsMaps = 0;
	
	for($i=1;$i<=$map_number;$i++) {
		$output .= '<div data-id="'.$i.'" class="stadtkreation-maps-map-number-box">';
		$output .= '<h2>'.sprintf(__('Map settings %s','stadtkreation-maps'),$i).'</h2>';
		foreach($stadtkreation_maps_fields as $stadtkreation_maps_field) {
			if($stadtkreation_maps_field['counter']==$i || !isset($stadtkreation_maps_field['counter'])) {
				if($stadtkreation_maps_field['title-before']) $output .= '<h3>'.sprintf($stadtkreation_maps_field['title-before'],$i).'</h3>';
				if($stadtkreation_maps_field['accordion']=='start') $output .= '<div class="settings-accordion">';
				if($stadtkreation_maps_field['text-before']) $output .= '<p class="text-before"><strong>'.sprintf($stadtkreation_maps_field['text-before'],$i).'</strong></p>';
				if($stadtkreation_maps_field['accordion']=='start') $output .= '<div class="settings-accordion-content">';
				if($stadtkreation_maps_field['object']) {
					$field_id = 'stadtkreation-maps-settings-'.$stadtkreation_maps_field['name'].'-'.$i.'-'.$stadtkreation_maps_field['object'];
					$field_name = 'stadtkreation-maps-settings['.$stadtkreation_maps_field['name'].']['.$i.']['.$stadtkreation_maps_field['object'].']';
					if(isset($stadtkreation_maps_setting[$stadtkreation_maps_field['name']])) $field_compare = $stadtkreation_maps_setting[$stadtkreation_maps_field['name']][$i][$stadtkreation_maps_field['object']];
					else $field_compare = '';
				}
				else {
					$field_id = 'stadtkreation-maps-settings-'.$stadtkreation_maps_field['name'].'-'.$i;
					$field_name = 'stadtkreation-maps-settings['.$stadtkreation_maps_field['name'].']['.$i.']';
					if(isset($stadtkreation_maps_setting[$stadtkreation_maps_field['name']])) $field_compare = $stadtkreation_maps_setting[$stadtkreation_maps_field['name']][$i];
					else $field_compare = '';
				}
				switch($stadtkreation_maps_field['type']) {
					case 'input':
						$output .= '<p><label for="'.$field_id.'">'.$stadtkreation_maps_field['label'].'<br /><input type="text" '.($stadtkreation_maps_field['reload'] ? ' onchange="location.reload()" ' : '').' value="'.($field_compare ? $field_compare : $stadtkreation_maps_field['default']).'" name="'.$field_name.'" id="'.$field_id.'" /></label></p>';
						break;
					case 'select':
						$output .= '<p><label for="'.$field_id.'">'.$stadtkreation_maps_field['label'].'<br /><select '.($stadtkreation_maps_field['reload'] ? ' onchange="location.reload()" ' : '').' name="'.$field_name.'" id="'.$field_id.'">';
						$current_value = $field_compare;
						if(!$current_value) $current_value = $stadtkreation_maps_field['default'];
						foreach($stadtkreation_maps_field['options'] as $field_option) $output .= '<option'.($current_value==$field_option['value'] ? ' selected="selected"' : '').' value="'.$field_option['value'].'">'.$field_option['name'].'</option>';
						$output .= '</select></label></p>';
						break;
					case 'select-multiple':
						$output .= '<p><label for="'.$field_id.'">'.$stadtkreation_maps_field['label'].'<br /><select multiple '.($stadtkreation_maps_field['reload'] ? ' onchange="location.reload()" ' : '').' name="'.$field_name.'[]" id="'.$field_id.'">';
						$current_value = $field_compare;
						if(!$current_value) $current_value = $stadtkreation_maps_field['default'];
						foreach($stadtkreation_maps_field['options'] as $field_option) $output .= '<option'.(isset($field_option['value']) && $current_value ? (in_array($field_option['value'],$current_value) ? ' selected="selected"' : '') : '').' value="'.$field_option['value'].'">'.$field_option['name'].'</option>';
						$output .= '</select></label></p>';
						break;
					case 'checkbox':
						$output .= '<p><label for="'.$field_id.'"><input type="checkbox"'.($field_compare=='on' ? ' checked="checked"' : '').' name="'.$field_name.'" /> '.$stadtkreation_maps_field['label'].'</label></p>';
						break;
					case 'map-poi':
						$output .= '<p><label for="'.$field_id.'">'.$stadtkreation_maps_field['label'];
						$output .= '<span class="stadtkreation-maps settings-map"></span>';
						$countSettingsMaps++;
						$given_lon = (isset($field_compare['lon']) ? $field_compare['lon'] : $stadtkreation_maps_field['default'][0]);
						$given_lat = (isset($field_compare['lat']) ? $field_compare['lat'] : $stadtkreation_maps_field['default'][1]);
						$output .= '<input type="hidden" class="stadtkreation-maps-position-lon'.$i.'" val="'.$given_lon.'" name="'.$field_name.'[lon]">';
						$output .= '<input type="hidden" class="stadtkreation-maps-position-lat'.$i.'" val="'.$given_lat.'" name="'.$field_name.'[lat]">';
						$output .= '</label><br />';
						$output .= '<script type="text/javascript"> if(typeof(settingsMapLon) === "undefined" || typeof(settingsMapLat) === "undefined") { var settingsMapLon=new Array(); var settingsMapLat=new Array(); } settingsMapLon.push('.$given_lon.'); settingsMapLat.push('.$given_lat.'); </script>'."\n";
						$output .= '<a class="reload-marker-position" href="javascript:placeCurrentIcon('.$given_lon.', '.$given_lat.',\'\',\'\','.$countSettingsMaps.')">'.__('Reload last saved marker position','stadtkreation-maps').'</a><br />'."\n";
						$output .= __('Current marker position (longitude, latitude)','stadtkreation-maps').': <strong class="stadtkreation_maps_show_current_position'.$i.'">'.__('No position set','stadtkreation-maps').'</strong><br />'."\n";
						break;
					case 'file':
						$output .= '<p><label for="'.$field_id.'">'.$stadtkreation_maps_field['label'].'<br /><input class="upload_image" type="text" name="'.$field_name.'" id="'.$field_id.'" value="'.(isset($field_compare) ? esc_attr($field_compare) : '').'" /><input class="upload_image_button button-secondary" class="button" type="button" value="'.__('Select File','stadtkreation-maps').'" /></p>';
						break;
					case 'image':
						$output .= '<p><label for="'.$field_id.'">'.$stadtkreation_maps_field['label'].'<br /><input class="upload_image" type="text" name="'.$field_name.'" id="'.$field_id.'" value="'.(isset($field_compare) ? esc_attr($field_compare) : '').'" /><input class="upload_image_button button-secondary" class="button" type="button" value="'.__('Select Image','stadtkreation-maps').'" />'.(isset($field_compare) && $field_compare != '' ? ' &nbsp; <span style="height:30px;overflow:visible;display:inline-block"><img src="'.esc_attr($field_compare).'" style="vertical-align:top;margin-top:-3px;max-height:60px;max-width:160px;" alt="'.__('selected image','stadtkreation-maps').'" /></span>' : '').'</p>';
						break;
					case 'color':
						$output .= '<p><label for="'.$field_id.'">'.$stadtkreation_maps_field['label'].'<br /><input class="colorfield" type="text" name="'.$field_name.'" id="'.$field_id.'" value="'.(isset($field_compare) ? esc_attr($field_compare) : '').'" />';
						break;
				}
				if($stadtkreation_maps_field['accordion']=='end') $output .= '</div></div>';
				if($stadtkreation_maps_field['title-after']) $output .= '<h3>'.sprintf($stadtkreation_maps_field['title-after'],$i).'</h3>';
			}
		}
		$output .= '</div>';
	}
	
	// hidden field for saving selected map settings tab
	$output .= '<input type="hidden" id="stadtkreation-maps-selected-tab" name="stadtkreation-maps-temp[selected-tab]" value="1" />';
	
	// output bottom submit button
	$output .= '<p class="submit"><input type="submit" class="button-primary" value="'.__('Save Changes').'" /></p>';
	
	// close form and wrapper
	$output .= '</form>';
	$output .= '</div>';
	
	// output settings fields
	echo $output;
}

function stadtkreation_maps_create_menu() {

	//create new top-level menu
	add_options_page(__('Map preferences','stadtkreation-maps'),__('Stadtkreation Maps','stadtkreation-maps'), 'manage_options', 'stadtkreation-maps-preferences', 'stadtkreation_maps_settings_page');
	
	//call register settings function
	add_action( 'admin_init', 'stadtkreation_maps_register_settings' );
}

// meta boxes
add_action( 'load-post.php', 'stadtkreation_maps_post_meta_boxes_setup' );
add_action( 'load-post-new.php', 'stadtkreation_maps_post_meta_boxes_setup' );

function stadtkreation_maps_post_meta_boxes_setup() {
	add_action( 'add_meta_boxes', 'stadtkreation_maps_add_post_meta_boxes' );
}

//Stadtkreation Maps metabox for post edit view
function stadtkreation_maps_add_post_meta_boxes() {
	global $count_maps;
	$posttypes = array();
	
	$stadtkreation_maps_setting = get_option('stadtkreation-maps-settings');
	
	for($i=1;$i<=$count_maps;$i++) {
		if(isset($stadtkreation_maps_setting['posttypes'][$i])) $posttypes = array_merge($stadtkreation_maps_setting['posttypes'][$i],$posttypes);
	}
	if(!$posttypes) $posttypes = explode('post',',');
	foreach($posttypes as $stadtkreation_maps_posttype) {
		add_meta_box(
			'stadtkreation-maps-post-class',
			esc_html__(__('Georeferencing','stadtkreation-maps'),__('Georeferencing','stadtkreation-maps')),
			'stadtkreation_maps_post_class_meta_box',
			$stadtkreation_maps_posttype,
			'normal',
			'default'
		);
	}
}

// edit post from backend
function stadtkreation_maps_post_class_meta_box($post) {
	global $count_maps;
	$output = '';
	$markerSet = false;
	if(is_numeric(get_post_meta($post->ID, 'stadtkreation-maps-position-lon', true )) && is_numeric(get_post_meta($post->ID, 'stadtkreation-maps-position-lat', true ))) $markerSet = true;
	wp_nonce_field( basename( __FILE__ ), 'stadtkreation_maps_post_class_nonce' );

	$output .= '<div class="stadtkreation-maps addpost" style="height:300px;"></div>'."\n";
	$output .= '<p>'.__('Current marker position (longitude, latitude)','stadtkreation-maps').': <strong class="stadtkreation_maps_show_current_position1">'.($markerSet ? get_post_meta($post->ID, 'stadtkreation-maps-position-lon', true ) : '').', '.($markerSet ? get_post_meta($post->ID, 'stadtkreation-maps-position-lat', true ) : '').(!$markerSet ? __('No position set','stadtkreation-maps') : '').'</strong><br />'."\n";
	$output .= '<span id="reset_marker_pos" style="display:none"><a href="javascript:placeCurrentIcon('.get_post_meta($post->ID, 'stadtkreation-maps-position-lon', true ).','.get_post_meta($post->ID, 'stadtkreation-maps-position-lat', true ).')">'.__('Reload last saved marker position','stadtkreation-maps').'</a></span><br />'."\n";
	if(get_post_meta($post->ID, 'stadtkreation-maps-position-lon', true )!='' && get_post_meta($post->ID, 'stadtkreation-maps-position-lat', true )!='') $output .= '<script type="text/javascript"> jQuery("#reset_marker_pos").css("display","inline"); jQuery("#delete_current_marker").css("display","inline"); </script>'."\n";
	$output .= '<p><label for="_stadtkreation-maps-post-vector-data">'.__('Vector data for this post (.geojson file)','stadtkreation-maps').'<br /><input class="upload_image" type="text" name="_stadtkreation-maps-post-vector-data" id="_stadtkreation-maps-post-vector-data" value="'.get_post_meta($post->ID,'_stadtkreation-maps-post-vector-data',true).'" /><input class="upload_image_button button-secondary" class="button" type="button" value="'.__('Select .geojson file','stadtkreation-maps').'" /></label></p>';
	$output .= '<input type="hidden" name="stadtkreation-maps-position-lon" class="stadtkreation-maps-position-lon1" value="'.get_post_meta($post->ID, 'stadtkreation-maps-position-lon', true ).'" />'."\n";
	$output .= '<input type="hidden" name="stadtkreation-maps-position-lat" class="stadtkreation-maps-position-lat1" value="'.get_post_meta($post->ID, 'stadtkreation-maps-position-lat', true ).'" />'."\n";
	$output .= '</p>'."\n";

	echo $output;
}

// save post from backend
add_action('save_post','stadtkreation_maps_post_save');
function stadtkreation_maps_post_save($post_id) {
	
  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
      return;
  if ( isset($_POST['stadtkreation_maps_post_class_nonce']))
	if(!wp_verify_nonce( $_POST['stadtkreation_maps_post_class_nonce'], basename( __FILE__ ) ) )
      return;
  // return if current user can't edit post
  if ( !current_user_can( 'edit_post', $post_id ) )
      return;
  
  // add post meta values
  if(isset($_POST['stadtkreation-maps-position-lat'])) if ( is_numeric( $_POST['stadtkreation-maps-position-lat'] )) update_post_meta($post_id, 'stadtkreation-maps-position-lat', $_POST['stadtkreation-maps-position-lat']);
  else delete_post_meta($post_id, 'stadtkreation-maps-position-lat');
  if(isset($_POST['stadtkreation-maps-position-lon'])) if ( is_numeric( $_POST['stadtkreation-maps-position-lon'] )) update_post_meta($post_id, 'stadtkreation-maps-position-lon', $_POST['stadtkreation-maps-position-lon']);
  else delete_post_meta($post_id, 'stadtkreation-maps-position-lon');
  $filetype = wp_check_filetype($_POST['_stadtkreation-maps-post-vector-data']);
  if($filetype['type']=='application/javascript' && $filetype['ext']=='geojson') update_post_meta($post_id, '_stadtkreation-maps-post-vector-data', $_POST['_stadtkreation-maps-post-vector-data']);
  else delete_post_meta($post_id, '_stadtkreation-maps-post-vector-data');
}

add_action('wp_footer', 'stadtkreation_maps_loader');
add_action('admin_footer', 'stadtkreation_maps_loader');


	
// load map
function stadtkreation_maps_loader() {
	global $count_maps, $is_overview, $overview_map_cat, $post, $map_number;
	echo '<script type="text/javascript">';
	
	$a = $map_number;
	if(!$a) $a = 1;
	
	$stadtkreation_maps_setting = get_option('stadtkreation-maps-settings');
	
	// create taxonomy selector
	if(isset($stadtkreation_maps_setting['taxonomy'][$a])) {
		echo 'taxonomySelectorHTML = "';
		$taxonomy_objects=get_categories( 'hide_empty=0&tab_index=4&taxonomy='.$stadtkreation_maps_setting['taxonomy'][$a].'&orderby=slug' );
		echo '<span class=\"selectbuttons\">';
		$countTaxonomies = 0;
		foreach($taxonomy_objects as $taxonomy_object)
			echo '<button class=\"bgcolor-'.$stadtkreation_maps_setting['taxonomy'][$a].$taxonomy_object->term_id.'\" onclick=\"iconSelectCat(this)\" name=\"'.$taxonomy_object->term_id.'\">'.($stadtkreation_maps_setting['taxonomy-usedesc'][$a]=='on' ? $taxonomy_object->description : $taxonomy_object->name).'</button>'; 
			$countTaxonomies++;
		if($countTaxonomies && $stadtkreation_maps_setting['taxonomy-others'][$a]=='on')  echo '<button onclick=\"iconSelectCat(this)\" name=\"empty\">'._x('Other','elements with empty category or custom taxonomy','stadtkreation-maps').'</button>';
		if($countTaxonomies) echo '</span><button onclick=\"toggleTaxonomySelector()\" class=\"mobiletoggle\"></button>';
		echo '";';
	}
	else echo 'taxonomySelectorHTML = ""; ';
	
	// create geosearch form
	if(isset($stadtkreation_maps_setting['taxonomy'][$a])) {
		echo 'geosearchFormHTML = "<form class=\"geosearchform hidden\"><input type=\"text\" name=\"addressfield\" class=\"addressfield\" /><input type=\"submit\" value=\"'.__('Search place','stadtkreation-maps').'\" /></form><button type=\"button\"></button>";';
	}
	else echo 'geosearchFormHTML = ""; ';
		
	echo 'jQuery(document).ready(function() { if(jQuery("#stadtkreation-maps1").length)  {'."\n\t".'stadtkreation_maps_init(); ';
	// check for correct lon and lat values
	$correct_lon = false;
	$correct_lat = false;
	
	// correct lon/lat check for form data
	if(isset($_POST['stadtkreation-maps-position-lon']) && $_POST['stadtkreation-maps-position-lon'] != '' && $_POST['stadtkreation-maps-position-lon'] >= -180 && $_POST['stadtkreation-maps-position-lon'] <= 180) $correct_lon = true;
	if(isset($_POST['stadtkreation-maps-position-lat']) && $_POST['stadtkreation-maps-position-lat'] != '' && $_POST['stadtkreation-maps-position-lat'] >= -90 && $_POST['stadtkreation-maps-position-lat'] <= 90) $correct_lat = true;
	if(isset($_POST['errorcheck']) && $correct_lon && $correct_lat) echo ' placeCurrentIcon('.$_POST['stadtkreation-maps-position-lon'].','.$_POST['stadtkreation-maps-position-lat'].');';
	
	if($is_overview) {
		global $post;
		$posttypes_query = 'post';
		if(isset($stadtkreation_maps_setting['posttypes'][$a]) && $stadtkreation_maps_setting['posttypes'][$a]!='') $posttypes_query = $stadtkreation_maps_setting['posttypes'][$a];
		$args = array('post_type' => $posttypes_query,'posts_per_page' => -1);
		if($overview_map_cat) $args['tax_query'] = array(
			array (
				'taxonomy' => $stadtkreation_maps_setting['taxonomy'][$a],
				'field' => 'id',
				'terms' =>  $overview_map_cat,
			)
		);
		$myposts = query_posts( $args );
		$countIcons=0;
		echo 'var detailText = "'.__('Details for this place','stadtkreation-maps').'";';
		foreach( $myposts as $post )  {
			// correct lon/lat check
			$correct_lon = false;
			$correct_lat = false;
			if(get_post_meta($post->ID,'stadtkreation-maps-position-lon',true) != '' && get_post_meta($post->ID,'stadtkreation-maps-position-lon',true) >= -180 && get_post_meta($post->ID,'stadtkreation-maps-position-lon',true) <= 180) $correct_lon = true;
			if(get_post_meta($post->ID,'stadtkreation-maps-position-lat',true) != '' && get_post_meta($post->ID,'stadtkreation-maps-position-lat',true) >= -90 && get_post_meta($post->ID,'stadtkreation-maps-position-lat',true) <= 90) $correct_lat = true;
		
			$icondata = singleMapDataTermsSettings($post->ID);
			$postImgSrc = '';
			if(get_the_post_thumbnail($post->ID)) {
				$postImg=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID,'thumbnail'));
				$postImgSrc = $postImg[0];
			}
			if(($correct_lon && $correct_lat) || (!get_post_meta($post->ID,'stadtkreation-maps-position-lon',true) && !get_post_meta($post->ID,'stadtkreation-maps-position-lat',true) && get_post_meta($post->ID,'_stadtkreation-maps-post-vector-data',true))) {
				// get post title with hook
				ob_start();
				the_title();
				$post_title = ob_get_clean();
				ob_flush();
				
				// check for post content display
				$post_content = '';
				if($stadtkreation_maps_setting['posttype-popover-content'][$a][$post->post_type]=='on') {
					if($stadtkreation_maps_setting['posttype-popover-content'][$a][$post->post_type]>0) $post_content = wp_trim_words($post->post_content,$stadtkreation_maps_setting['posttype-popover-content'][$a][$post->post_type]);
					else {
						ob_start();
						the_content();
						$post_content = ob_get_clean();
						ob_end_flush();
					}
				}
				
				// define output for JS vector style array
				$vector_style_array = '[';
				if(isset($icondata[2])) foreach($icondata[2] as $single_style_setting) $vector_style_array .= '"'.$single_style_setting.'",';
				$vector_style_array .= ']';
				
				$lon = (get_post_meta($post->ID,'stadtkreation-maps-position-lon',true) ? get_post_meta($post->ID,'stadtkreation-maps-position-lon',true) : "0");
				$lat = (get_post_meta($post->ID,'stadtkreation-maps-position-lat',true) ? get_post_meta($post->ID,'stadtkreation-maps-position-lat',true) : "0");
				
				echo 'addSingleMapData('.$lon.','.$lat.',"'.addslashes($post_title).'",'.$countIcons.',"'.get_permalink($post->ID).'","'.$icondata[0].'","'.$icondata[1].'","'.$postImgSrc.'",'.json_encode('<div class="'.$post->post_type.'">'.$post_content.'</div>').',"'.$stadtkreation_maps_setting['posttype-popover-nodetails'][$a][$post->post_type].'","'.get_post_meta($post->ID,'_stadtkreation-maps-post-vector-data',true).'",'.$vector_style_array.');'."\n";
				$countIcons++;
			}
		}
	}
	else {
	
		$icondata = singleMapDataTermsSettings($post->ID);
		
		// place current icon for single edit pages
		if(get_post_meta($post->ID, 'stadtkreation-maps-position-lon', true ) && get_post_meta($post->ID, 'stadtkreation-maps-position-lat', true )) echo 'placeCurrentIcon('.get_post_meta($post->ID, 'stadtkreation-maps-position-lon', true ).','.get_post_meta($post->ID, 'stadtkreation-maps-position-lat', true ).',"'.$icondata[1].'","'. get_post_meta($post->ID, '_stadtkreation-maps-post-vector-data', true ).'");'."\n";

		// place current icon in each map within plugin settings page
		if(!$post->ID) echo ' if(settingsMapLon.length) for(var i=0;i<settingsMapLon.length;i++) placeCurrentIcon(settingsMapLon[i],settingsMapLat[i],"","",i+1);'."\n";
	}
	echo '} }) </script>';


}

function singleMapDataTermsSettings($postid) {
	global $map_number;

	$a = $map_number;
	if(!$a) $a = 1;
	
	$stadtkreation_maps_setting = get_option('stadtkreation-maps-settings');
	
	$current_term = '';
	if(isset($stadtkreation_maps_setting['taxonomy'][$a])) $current_term = $stadtkreation_maps_setting['taxonomy'][$a];
	
	$output_cat = '';
	$src = '';
	$terms = get_the_terms($postid,$current_term);
	if($terms) {
		$current_term_id = '';
		foreach($terms as $term) { 
			$check_term = get_term($term,$current_term);
			if($check_term) {
				$current_term = $term;
				break;
			}
		}
		if($current_term) 
		$current_term_slug = $current_term->slug;
		$current_term_id = $current_term->term_id;
		$output_cat = $current_term_id;
		
		// getting the taxonomy icon
		if($current_term_slug) {
			$src = $stadtkreation_maps_setting['taxonomy-icon'][$a][$current_term->term_id];
		}
		
		// getting the taxonomy vector style data
		$taxonomy_styles = array(
			$stadtkreation_maps_setting['taxonomy-stroke-color'][$a][$current_term->term_id],
			$stadtkreation_maps_setting['taxonomy-stroke-opacity'][$a][$current_term->term_id],
			$stadtkreation_maps_setting['taxonomy-stroke-width'][$a][$current_term->term_id],
			$stadtkreation_maps_setting['taxonomy-stroke-style'][$a][$current_term->term_id],
			$stadtkreation_maps_setting['taxonomy-fill-color'][$a][$current_term->term_id],
			$stadtkreation_maps_setting['taxonomy-fill-opacity'][$a][$current_term->term_id],
		);
	}
	else $output_cat='empty'; 
	return array($output_cat,$src,$taxonomy_styles);
}

// shortcode [stadtkreation-map]
function stadtkreation_maps_output($atts){
	global $count_maps, $is_overview, $overview_map_cat, $map_number;
	$a = shortcode_atts( array(
        'number' => 1,
		'cat' => '',
    ), $atts );
	$map_number = $a['number'];
	$is_overview = true;
	$overview_map_cat = $a['cat'];
	$output = '<script type="text/javascript"> var mapNumber = '.($a['number']).'; </script>';
	$output .= '<div class="stadtkreation-maps overview"></div>'."\n";
	return $output;
}
add_shortcode('stadtkreation-map','stadtkreation_maps_output');

function stadtkreation_maps_single_add($content){
	global $post, $count_maps;
	$posttypes = array();
	
	$stadtkreation_maps_setting = get_option('stadtkreation-maps-settings');
	
	$map_number = 1;
	for($i=1;$i<=$count_maps;$i++) {
		if(isset($stadtkreation_maps_setting['posttypes'][$i])) {
			$posttypes = array_merge($stadtkreation_maps_setting['posttypes'][$i],$posttypes);
			if(in_array(get_post_type($post),$stadtkreation_maps_setting['posttypes'][$i])) $map_number = $i;
		}
	}
	if(!$posttypes) $posttypes = explode('post',',');
	if(is_single() && get_post_meta($post->ID, 'stadtkreation-maps-position-lon', true ) && get_post_meta($post->ID, 'stadtkreation-maps-position-lat', true )) {
		if(in_array(get_post_type($post),$posttypes)) {	
			$output = $content;
			$output .= '<script type="text/javascript"> var mapNumber = '.$map_number.'; </script>' . "\n";
			$output .= '<p><span class="stadtkreation-maps single" style="height:300px;"></span></p>'."\n";
			$output .= '<script type="text/javascript"> jQuery(document).ready(function(){ placeCurrentIcon('.get_post_meta($post->ID, 'stadtkreation-maps-position-lon', true ).','.get_post_meta($post->ID, 'stadtkreation-maps-position-lat', true ).'); }); </script>';
			return $output;
		}
		else return $content;
	}
	else return $content;
}
add_filter('the_content','stadtkreation_maps_single_add');

// allow json upload {
add_filter('upload_mimes', 'stadtkreation_maps_upload_mimes');
function stadtkreation_maps_upload_mimes ( $existing_mimes=array() ) {
	// add your extension to the array
	$existing_mimes['json'] = 'application/geo+json';
	$existing_mimes['geojson'] = 'application/geo+json';

	// and return the new full result
	return $existing_mimes;
}

function stadtkreation_maps_admin_scripts( ) {
	global $post;
	// preparing the media uploader
	wp_enqueue_media();
	
	// preparing the color picker
    wp_enqueue_style( 'wp-color-picker' );
	wp_enqueue_script( 'wp-color-picker');
	
	wp_enqueue_script( 'stadtkreation-maps-admin-script', STADTKREATON_MAPS_PLUGIN_URL.'/admin-functions.js', array('wp-color-picker'), false, true );

		
}
add_action( 'admin_enqueue_scripts', 'stadtkreation_maps_admin_scripts' );
?>